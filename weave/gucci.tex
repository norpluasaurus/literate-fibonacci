\documentclass{article}% ===> this file was generated automatically by noweave --- better not edit it
\usepackage{noweb}
\usepackage{hyperref}

\title{Fibonacci Sequence}
\author{Nora Cook}

\begin{document}
\pagestyle{noweb}
\maketitle
\newpage
\tableofcontents
\newpage

\section{Introduction}
In 1984 Donald E. Knuth wrote his essay \textit{Literate Programming} which
espoused a new programming paradigm, \textbf{literate programming}. The
idea behind this was that programs are not just read by computers, they are
also read by humans. Knuth felt that because of this we need to write
software as literature.

To accomplish this Knuth created the \textit{WEB} system, written in Pascal.
He eventually used this program to create both \TeX{} and \textit{METAFONT}.
Following the creation of \textit{WEB}, Donald Knuth and Silvio Levy created
the \textit{CWEB} program with C in 1987. While we will not be using
\textit{CWEB}, it is a very well known tool for literate programming if you wish
to pursue it further.

\textit{Noweb} will be the program we are using, it actually consists of two
separate tools, \texttt{notangle} and \texttt{noweave}, but we will go over
that soon enough. So without further ado, let's begin!

\section{Getting Set Up}
Before we can start we must have a proper development environment and lucky
enough for you, I already have one all set up. To begin, use an ssh client
either through the terminal or with something like \textit{PuTTY}. You will
want to login as \texttt{user} plus the number assigned to you, e.g. if you
are number 12 your username would be \texttt{user12}. The site to ssh into is
\texttt{noracook.io}, once logged in you will be in your home directory and
will be able to access all files and programs you may need for this project.
In regards to text editors, there is \texttt{vim|nano|emacs}, use what you
are most comfortable with.

For a Linux command line cheat sheet, visit \href{https://files.fosswire.com/2007/08/fwunixref.pdf}{here}.

There also should be a {\Tt{}Makefile\nwendquote} provided to you, it is very simple and all
it does is aggregate the various steps of ``compiling'' a noweb program.

\section{The Program}
\subsection{Starting Out}
\subsubsection{Includes}
\nwfilename{src/gucci.nw}\nwbegindocs{1}Including and importing are an essential part of software development, and
literate programming makes it even better. By having the ability to define a
section using {\Tt{}\LA{} Section name \RA{}=\nwendquote} and invoking it using
{\Tt{}\LA{} Section Name \RA{}\nwendquote} makes it easy to keep track of numerous includes.

\nwenddocs{}\nwbegincode{2}\moddef{Headers}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
#include <stdio.h>
\nwendcode{}\nwbegindocs{3}\nwdocspar

\subsubsection{The Main Method}
\nwenddocs{}\nwbegindocs{4}Since we are writing in C we must of course have a main method, this will
also be where we define the name of our programs output file. This is simply
done by declaring a section name with the name of the file,
{\Tt{}<<file.c\nwendquote}. Within here we will place both our headers, and make a skeleton
of our program. The beauty of literate programming is that we can define and
describe a feature of the program beforehand and easily be able to flesh it out
later on. Here in our main method we want three things, to initialize
variables prompt and retrieve a number, and run our fibonacci function. We can
write these now to better be able to structure our program in a cohesive way.
This shift in thinking also helps think of software more holistically. We can
also append our future Fibonacci function into the code with a section tag.

\nwenddocs{}\nwbegincode{5}\moddef{gucci.c}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup

\LA{}Headers\RA{}

int main() \{
    \LA{}Initialize variables\RA{}
    \LA{}Prompt and retrieve number\RA{}
    \LA{}Run Fibonacci function\RA{}

    return 0;
\}

\LA{}Fibonacci function\RA{}
\nwendcode{}\nwbegindocs{6}\nwdocspar

\subsubsection{The Variables}
\nwenddocs{}\nwbegindocs{7}We only need one variable defined in the main method, but this is a good way
to showcase what is possible with something like \textit{noweb}. Also we need
to put this variable here so we have it available to store our number in.

\nwenddocs{}\nwbegincode{8}\moddef{Initialize variables}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    int input_number;
\nwendcode{}\nwbegindocs{9}\nwdocspar

\subsubsection{Reading in the Number}

\nwenddocs{}\nwbegindocs{10}Here all we need to do is simply print out a prompt, and then use {\Tt{}scanf\nwendquote}
to grab the inputted number and store it in {\Tt{}input{\_}number\nwendquote}.

\nwenddocs{}\nwbegincode{11}\moddef{Prompt and retrieve number}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    printf("Please enter a number\\n");
    scanf("%d", &input_number);
\nwendcode{}\nwbegindocs{12}\nwdocspar

\subsection{The Fibonacci Function}

\nwenddocs{}\nwbegindocs{13}While this is very superfluous it is another example of ways you can use
literate programming to atomize and structure your code.
\nwenddocs{}\nwbegincode{14}\moddef{Run Fibonacci function}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    fibonacci(input_number);
\nwendcode{}\nwbegindocs{15}\nwdocspar

\subsubsection{Back to the Top}

\nwenddocs{}\nwbegindocs{16}At the beginning we defined the section {\Tt{}\LA{}Headers\RA{}\nwendquote} and by invoking it
again, we are able to append to it. Using this feature we can add the function
prototype to the top of the page.

\nwenddocs{}\nwbegincode{17}\moddef{Headers}\plusendmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    void fibonacci(int input_number);
\nwendcode{}\nwbegindocs{18}\nwdocspar

\subsection{The Actual Fibonacci Function}

\nwenddocs{}\nwbegindocs{19}This is where we want to define the basics of our function, as with the
{\Tt{}main\nwendquote} function, we can describe the future features that we want. In this
case it's more initial variables, and a loop to do the math.

\nwenddocs{}\nwbegincode{20}\moddef{Fibonacci function}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    void fibonacci(int input_number) \{
        \LA{}Fibonacci variables\RA{};
        \LA{}Fibonacci loop\RA{};
    \}
\nwendcode{}\nwbegindocs{21}\nwdocspar

\subsubsection{Fibonacci's Variables}

\nwenddocs{}\nwbegindocs{22}Let's just get some basic variables out of the way, {\Tt{}first\nwendquote} is set to zero
and {\Tt{}second\nwendquote} is set to one because since we aren't using recursion, we need
to define a starting point. We also have {\Tt{}next\nwendquote} which acts as storage for the
variable that was just calculated to determine what the next one would be.
Lastly we have {\Tt{}count\nwendquote} which will be the counter for our {\Tt{}for\nwendquote} loop.

\nwenddocs{}\nwbegincode{23}\moddef{Fibonacci variables}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    int first = 0;
    int second = 1;
    int next;
    int count;
\nwendcode{}\nwbegindocs{24}\nwdocspar

\subsubsection{Loop Logic}

\nwenddocs{}\nwbegindocs{25}Even a basic {\Tt{}for\nwendquote} loop can be broken down, and while there is a limit to
how much you can atomize something in IT, breaking down problems can help solve
a problem very well. Using {\Tt{}counter\nwendquote} that was defined before we set up a
basic for loop, starting from zero and incrementing by one each time. We also
have the check set against {\Tt{}input{\_}number\nwendquote} so it has both a start point from
our initialized variables {\Tt{}first\nwendquote} and {\Tt{}second\nwendquote} but we also have a stopping
point.

Within the loop we define one section for the logic and another for the simple
act of printing out the resolved numbers.

\nwenddocs{}\nwbegincode{26}\moddef{Fibonacci loop}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    for(count = 0; count < input_number; count++) \{
        \LA{}Fibonacci logic\RA{};
        \LA{}Print numbers\RA{};
    \}
\nwendcode{}\nwbegindocs{27}\nwdocspar

\subsubsection{If's and Else's}

\nwenddocs{}\nwbegindocs{28}It's always easier to work with less code, and literate programming aids in
this endeavor by allowing you to work on small chunks of code like this
section.

Firstly we want to check what number the count is at, the first two passes it
will be zero, each time giving its value to {\Tt{}next\nwendquote}. Once {\Tt{}count\nwendquote} equals two
the else loop is used. In here we first derive the next number by adding the
{\Tt{}first\nwendquote} and {\Tt{}second\nwendquote}, then we transfer the value of the second into the
first, this increments the first into the next number in the sequence, the same
thing happens with {\Tt{}second\nwendquote} and {\Tt{}next\nwendquote} with the latter giving its value to
the former.

\nwenddocs{}\nwbegincode{29}\moddef{Fibonacci logic}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    if(count <= 1) \{
        next = count;
    \}
    else \{
        next = first + second;
        first = second;
        second = next;
    \}
\nwendcode{}\nwbegindocs{30}\nwdocspar

\nwenddocs{}\nwbegindocs{31}Finally we are ready to print out our fancy math problem, we use  a simple
{\Tt{}printf\nwendquote} function and pass in {\Tt{}next\nwendquote} to print out each iteration of the
numbers.

\nwenddocs{}\nwbegindocs{32}Now print it out
\nwenddocs{}\nwbegincode{33}\moddef{Print numbers}\endmoddef\nwstartdeflinemarkup\nwenddeflinemarkup
    printf("%d\\n", next);
\nwendcode{}\nwbegindocs{34}\nwdocspar

\section{The Conclusion}
\nwenddocs{}\nwbegindocs{35}To compile this, simply type make on the command line and all the necessary
files will be outputted and be accessible.

\end{document}
\nwenddocs{}
