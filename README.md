# Capstone Presentation
## Nora Cook
Simple Fibonacci Numbers program written in C using noweb.

Not meant to be a spectacular program, just used as medium to explain
literate programming.

See the pdf in the docs folder for the complete, compiled program.
