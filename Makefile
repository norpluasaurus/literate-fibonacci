default:
	noweave -delay src/gucci.nw > weave/gucci.tex
	latexmk -outdir=docs/ -pdf weave/gucci.tex
	notangle -L -Rgucci.c src/gucci.nw > tangle/gucci.c
	cc tangle/gucci.c -o bin/gucci
