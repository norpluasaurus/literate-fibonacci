\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Getting Set Up}{3}{section.2}
\contentsline {section}{\numberline {3}The Program}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Starting Out}{3}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Includes}{3}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}The Main Method}{4}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}The Variables}{4}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Reading in the Number}{4}{subsubsection.3.1.4}
\contentsline {subsection}{\numberline {3.2}The Fibonacci Function}{5}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Back to the Top}{5}{subsubsection.3.2.1}
\contentsline {subsection}{\numberline {3.3}The Actual Fibonacci Function}{5}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Fibonacci's Variables}{5}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Loop Logic}{6}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}If's and Else's}{6}{subsubsection.3.3.3}
\contentsline {section}{\numberline {4}The Conclusion}{7}{section.4}
